<?php

// This is just an example of reading server side data and sending it to the client.
// It reads a json formatted text file and outputs it.


$link = mysqli_connect("localhost","root","","charts");

$query = "SELECT * FROM `country`";
$result = $link->query( $query );

$data = array();

$data['cols'][] = array('label' => 'Страна', 'type' => 'string');
$data['cols'][] = array('label' => 'Население', 'type' => 'number');
$i = 0;

while($row = mysqli_fetch_assoc($result)){
    $data['rows'][$i]['c'][0]['v'] = $row['country_name'];
    $data['rows'][$i]['c'][1]['v'] = 0;
    $data['rows'][$i]['c'][1]['r'] = (int)$row['population'];
    $i++;
}

echo json_encode( $data );










