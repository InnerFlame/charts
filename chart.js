// Load the Visualization API and the piechart package.
google.load('visualization', '1', {'packages':['corechart']});

// Set a callback to run when the Google Visualization API is loaded.
google.setOnLoadCallback(drawChart);

function drawChart() {
    var res;
    var jsonData = $.ajax({
        url: "getData.php",
        dataType:"json",
        async: false,
        success: function(response){
            res = response;
        }
    }).responseText;

    // Create our data table out of JSON data loaded from server.
    var data = new google.visualization.DataTable(jsonData);

    var options = {
        'title':'Кол',
        'width':800,
        'height':500,
        'backgroundColor':{
            'strokeWidth':2
        },
        'animation':{
            'duration':1000,
            'easing':'out'
        },
        'vAxis':{
            'minValue':0,
            'maxValue':300
        }
    };

    // Instantiate and draw our chart, passing in some options.
    var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
    chart.draw(data, options);

setTimeout(function(){
//    res = JSON.parse(res);
    var length = res['rows'].length;
    for(var i = 0; i < length; i++){
        data.setValue( i, 1, res['rows'][i]['c'][1]['r']);
    }
    chart.draw(data, options);
}, 500);



}